<?php

namespace App\Http\Controllers;

use App\Almanac;
use App\BibleReading;
use Illuminate\Http\Request;

class AlmanacController extends Controller
{
    //
    public function store(Request $request){
        $almanac= new Almanac();

        $almanac->occassion =  $request->input('almanacOccassion');
        $almanac->date= $request->input('almanacDate');
        $almanac->theme= $request->input('almanacTheme');
        $almanac->preacher= $request->input('preacher');
        $almanac->save();
        foreach ($request->input('bibleReading') as $item){
            $reading= new BibleReading();
            //dd($item);
            $reading->almanac_id = $almanac->id;
            $reading->bible_reading = $item;
            $reading->save();
        }
        return redirect()->back();
    }

    public function index(){
        $almanac= Almanac::whereHas('readings')
            ->with('readings')
            ->get();
        return view('almanac.index', compact('almanac'));
    }
}
