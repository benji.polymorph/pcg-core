<?php

namespace App\Http\Controllers;

use App\Member;
use App\Tithe;
use Illuminate\Http\Request;

class TitheController extends Controller
{
    //
    public function tithe($id)
    {
        $data=Tithe::where('memberID',$id)->get();
        return view('member.tithe', compact('id'));
    }

    public function store(Request $request)
    {
        $tithe= new Tithe();
        $tithe->amount=$request->input('amount');
        $tithe->memberID=$request->input('memberID');
        $tithe->save();
        return redirect()->back();
    }

    public function ajaxGetmemberTithe ($id){
        if($id!='all'){
            $data=[
                'data'=>Tithe::where('memberID',$id)
                ->with('member')
                ->get()
            ];
        }
        else{
            $data=[
                'data'=>Tithe::whereHas('member')
                ->with('member')->get()
            ];
        }
        return json_encode($data);
    }

}
