<?php

namespace App\Http\Controllers;

use App\Member;
use Illuminate\Http\Request;

class MemberController extends Controller
{

    public function index()
    {
        //
        return view('home');
    }


    public function create()
    {
        $data= new Member();
        return view('member.create',compact('data'));
    }

    public function familyTree()
    {
        //
        return view('member.tree');
    }

    public function ajaxloadMembers(){
        return json_encode(Member::all());
    }


    public function store(Request $request)
    {
        //
        //dd($request->input());
        if($request->input('id')){
            $table= Member::find($request->input('id'));
        }
        else{
            $table= new Member();

        }
        $table->gender=$request->input('gender');
        $table->surname=$request->input('surname');
        $table->otherNames=$request->input('otherNames');
        $table->firstName=$request->input('firstName');
        $table->phone=$request->input('phone');
        $table->email=$request->input('email');
        $table->residentialAddress=$request->input('residentialAddress');
        $table->generationalGroup=$request->input('generationalGroup');
        $table->occupation=$request->input('occupation');
        $table->dateOfBirth=$request->input('dateOfBirth');
        $table->maritalStatus=$request->input('maritalStatus');
        $table->educationalLevel=$request->input('educationalLevel');
        $table->emergencyContactTelephoneNumber=$request->input('emergencyContactTelephoneNumber');
        $table->emergencyContactRelationship=$request->input('emergencyContactRelationship');
        $table->nextOfKinTelephoneNumber=$request->input('nextOfKinTelephoneNumber');
        $table->nextOfKinRelationship=$request->input('nextOfKinRelationship');
        $table->photograph=$request->input('photograph');
        $table->postalAddress=$request->input('postalAddress');
        $table->baptismStatus=$request->input('baptismStatus');
        $table->communicantStatus=$request->input('communicantStatus');


        $table->emergencyContactName=$request->input('emergencyContactName');
        $table->nextOfKinName=$request->input('nextOfKinName');
        $table->save();

        return redirect()->back();

    }


    public function show(Member $member)
    {
        //
    }

    public function list()
    {
        //
        return view('member.list');
    }

    public function edit($id)
    {
        //
        $data=Member::find($id);
        return view('member.create', compact('data'));
    }

    public function tithe($id)
    {
        //
        $data=Member::find($id);
        return view('member.create', compact('data'));
    }


    public function ajaxGetmembers(){
        $data=[
            'data'=>Member::all()
        ];
        return json_encode($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Member $member)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function destroy(Member $member)
    {
        //
    }
}
