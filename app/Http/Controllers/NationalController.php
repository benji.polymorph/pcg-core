<?php

namespace App\Http\Controllers;

use App\National;
use Illuminate\Http\Request;

class NationalController extends Controller
{
    //
    public function getNational($id){

    }


    public function store(Request  $request){
        if(!$request->input('id')){
            $form= new National();
        }
        else{
            $form = National::find($request->input('id'));
        }
        $form->national_country= $request->input('national_country');
        $form->national_name= $request->input('national_name');
        $form->national_description= $request->input('national_description');
        $form->national_logo= $request->input('national_logo');
        $form->save();
        return redirect()->back();
    }

    public function getNationalsListAjax(){
        return json_encode(National::all('id','national_name','national_country'));
    }


    public function index(){
        return view('national.index');
    }


    public function getNationalAjax(){
        $data=[
            'data'=>National::all()
        ];
        return json_encode($data);
    }
}
