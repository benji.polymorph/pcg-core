<?php

namespace App\Http\Controllers;

use App\National;
use App\Presbytery;
use Illuminate\Http\Request;

class PresbyteryController extends Controller
{
    //
    public function getPresbytery($id){

    }

    public function store(Request  $request){
        //dd($request->input('id'));;
        if(!$request->input('id')){
            $form= new Presbytery();
        }
        else{
            $form = Presbytery::find($request->input('id'));
        }
        $form->presbytery_location= $request->input('presbytery_location');
        $form->presbytery_name= $request->input('presbytery_name');
        $form->presbytery_description= $request->input('presbytery_description');
        $form->presbytery_national= $request->input('presbytery_national');
        $form->save();
        return redirect()->back();
    }

    public function index(){
        $nationals=National::all();
        return view('presbytery.index',compact('nationals'));
    }

    public function getPresbyteryAjax(){
        $data=[
            'data'=>Presbytery::all()
        ];
        return json_encode($data);
    }
}
