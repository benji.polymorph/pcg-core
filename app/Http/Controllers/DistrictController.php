<?php

namespace App\Http\Controllers;

use App\District;
use App\Local;
use App\Presbytery;
use Illuminate\Http\Request;

class DistrictController extends Controller
{
    //
    public function getDistrict($id){

    }

    public function store(Request  $request){
        if(!$request->input('id')){
            $form= new District();
        }
        else{
            $form = District::find($request->input('id'));
        }
        $form->district_location= $request->input('district_location');
        //$form->district_location= $request->input('district_location');
        $form->district_name= $request->input('district_name');
        $form->district_description= $request->input('district_description');
        $form->district_presbytery= $request->input('district_presbytery');
        $form->save();
        return redirect()->back();
    }


    public function index(){
        $presbyteries= Presbytery::all();
        return view('district.index',compact('presbyteries'));
    }

    public function getDistrictAjax(){
        $data=[
            'data'=>District::all()
        ];
        return json_encode($data);
    }
}
