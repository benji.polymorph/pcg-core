<?php

namespace App\Http\Controllers;

use App\Announcement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AnnouncementController extends Controller
{
    //
    public function index(){
        return view('announcement.index');
    }

    public function store(Request $request){
        if($request->input('id')){
            $announcement= Announcement::find($request->input('id'));
        }
        else{
            $announcement= new Announcement();
        }
        $announcement->announcement_user=$request->input('announcement_user');
        $announcement->announcement_user=Auth::user()->id;
        $announcement->announcement_title=$request->input('announcement_title');
        $announcement->announcement_content=$request->input('announcement_content');
        $announcement->save();
    }

}
