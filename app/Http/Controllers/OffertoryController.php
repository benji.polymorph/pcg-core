<?php

namespace App\Http\Controllers;

use App\Offertory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OffertoryController extends Controller
{
    //
    public function ajaxGetoffertory(){
        $data=[
            'data'=>Offertory::whereHas('user')
                ->with('user')
                ->get()
        ];
        return json_encode($data);
    }

    public function offertory(){
        return view('offertory.index');

    }

    public function store(Request $request){
        $table= new Offertory();
        $id=Offertory::count()+100001;
        $table->userID= Auth::user()->id;
        $table->offertoryType= $request->input('offertoryType');
        $table->offertoryAmount= $request->input('offertoryAmount');
        $table->offertoryDescription= $request->input('offertoryDescription');
        $table->offertoryID= "OFF". $id;
        $table->save();

        return redirect()->back();
    }
}
