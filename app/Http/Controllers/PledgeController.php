<?php

namespace App\Http\Controllers;

use App\Pledge;
use Illuminate\Http\Request;

class PledgeController extends Controller
{
    //

    public function pledge($id)
    {
        //$data=Pledge::where('memberID',$id)->get();
        return view('member.pledge', compact('id'));
    }

    public function store(Request $request)
    {
        $pledge= new Pledge();
        $pledge->pledgeAmount=$request->input('pledgeAmount');
        $pledge->memberID=$request->input('memberID');
        $pledge->description=$request->input('description');
        $pledge->payment=0;
        $pledge->status="Pending";
        $pledge->save();
        return redirect()->back();
    }

    public function ajaxGetmemberPledge($id){
        if($id=="all"){
            $data=[
                'data'=>Pledge::whereHas('member')
                    ->with('member')
                    ->get()
            ];
        }
        else{
            $data=[
                'data'=>Pledge::where('memberID',$id)
                    ->with('member')
                    ->get()
            ];
        }
        return json_encode($data);
    }
}
