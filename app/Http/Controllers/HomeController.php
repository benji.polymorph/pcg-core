<?php

namespace App\Http\Controllers;

use App\District;
use App\Local;
use App\National;
use App\Presbytery;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function getTerritory($type)
    {
        //$data=[];
        //dd($type);
        switch ($type){
            case 'district_admin':
                $data=District::all('district_name as name');
                break;
                //dd($data);
            case 'local_admin':
                $data=Local::all('local_name as name');
                break;

            //dd($data);
            case 'presbytery_admin':
                $data=Presbytery::all('presbytery_name as name');
                break;

            case 'national_admin':
                $data=National::all('national_name as name');
                break;

            case 'super_admin':
                $data=[];
                break;

        }
        return json_encode($data);


    }
}
