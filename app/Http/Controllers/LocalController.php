<?php

namespace App\Http\Controllers;

use App\District;
use App\Local;
use App\National;
use Illuminate\Http\Request;

class LocalController extends Controller
{
    //
    public function getLocal($id){

    }


    public function store(Request  $request){
        if(!$request->input('id')){
            $form= new Local();
        }
        else{
            $form = Local::find($request->input('id'));
        }
        $form->local_location= $request->input('local_location');
        $form->local_name= $request->input('local_name');
        $form->local_description= $request->input('local_description');
        $form->local_district= $request->input('local_district');
        $form->save();
        return redirect()->back();
    }
    public function index(){
        $districts=District::all();
        return view('local.index',compact('districts'));
    }

    public function getLocalAjax(){
        $data=[
            'data'=>Local::all()
        ];
        return json_encode($data);
    }
}
