<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pledge extends Model
{
    //
    public function member(){
        return $this->belongsTo('App\Member','memberID','id');
    }
}
