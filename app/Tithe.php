<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tithe extends Model
{
    //
    public function member(){
        return $this->belongsTo('App\Member','memberID','id');
    }
}
