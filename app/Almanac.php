<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Almanac extends Model
{
    //
    public function readings(){
        return $this->hasMany('App\BibleReading','almanac_id','id');
    }
}
