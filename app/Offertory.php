<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offertory extends Model
{
    //
    public function user(){
        return $this->belongsTo('App\User','userID','id');
    }
}
