<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffertoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offertories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('offertoryID');
            $table->decimal('offertoryAmount',20,2);
            $table->string('offertoryType');
            $table->string('userID');
            $table->string('offertoryDescription');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offertories');
    }
}
