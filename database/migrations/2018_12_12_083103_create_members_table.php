<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');
            $table->string('surname');
            $table->string('otherNames')->nullable();
            $table->string('firstName');
            $table->string('gender');
            $table->string('phone');
            $table->string('email')->nullable();
            $table->string('generationalGroup');
            $table->string('residentialAddress')->nullable();
            $table->string('photograph')->nullable();
            $table->string('postalAddress');
            $table->string('maritalStatus');
            $table->string('dateOfBirth');
            $table->string('educationalLevel');
            $table->string('occupation');
            $table->string('emergencyContactName');
            $table->string('emergencyContactTelephoneNumber');
            $table->string('emergencyContactRelationship');
            $table->string('nextOfKinName');
            $table->string('nextOfKinTelephoneNumber');
            $table->string('nextOfKinRelationship');
            $table->string('baptismStatus');
            $table->string('communicantStatus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
