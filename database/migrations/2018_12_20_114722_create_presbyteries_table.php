<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePresbyteriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presbyteries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('presbytery_national');
            $table->string('presbytery_name');
            $table->string('presbytery_location');
            $table->string('presbytery_description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presbyteries');
    }
}
