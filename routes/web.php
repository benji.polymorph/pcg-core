<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();


Route::get('/', 'HomeController@index')->name('/');
Route::get('/getTerritory/{id}', 'HomeController@getTerritory')->name('getTerritory');



Route::get('/home', 'HomeController@index')->name('home');
Route::get('/member', 'MemberController@index')->name('member');
Route::get('/mem/create', 'MemberController@create')->name('member.create');
Route::get('/mem/edit/{id}', 'MemberController@edit')->name('member.edit');
Route::get('/loadMembers', 'MemberController@ajaxloadMembers')->name('ajax.loadMembers');



Route::get('/almanac', 'AlmanacController@ajaxloadAlmanac')->name('ajax.loadAlmanac');
Route::post('/almanac', 'AlmanacController@store')->name('almanac.store');
Route::get('/almanac', 'AlmanacController@index')->name('almanac.index');



Route::get('/mem/tithe/{id}', 'MemberController@tithe')->name('member.tithe');


Route::post('/mem/create', 'MemberController@store')->name('member.store');
Route::get('/member/list', 'MemberController@list')->name('member.list');
Route::get('/member/tree', 'MemberController@familyTree')->name('member.tree');
Route::get('/ajax/get-members', 'MemberController@ajaxGetmembers')->name('ajax.get-members');



Route::get('/ajax/get-offertory', 'OffertoryController@ajaxGetoffertory')->name('ajax.get-offertory');
Route::get('/offertory', 'OffertoryController@offertory')->name('offertory');
Route::post('/offertory', 'OffertoryController@store')->name('offertory.store');



Route::get('/ajax/get-memberPledge/{id}', 'PledgeController@ajaxGetmemberPledge')->name('ajax.get-memberPledge');
Route::get('/mem/pledge/{id}', 'PledgeController@pledge')->name('member.pledge');
Route::post('/mem/pledge', 'PledgeController@store')->name('pledge.store');



Route::get('/ajax/get-memberTithe/{id}', 'TitheController@ajaxGetmemberTithe')->name('ajax.get-memberTithe');
Route::get('/mem/tithe/{id}', 'TitheController@tithe')->name('member.tithe');
Route::post('/mem/tithe', 'TitheController@store')->name('tithe.store');


/*National routes*/
Route::get('national/{id}','NationalController@getNational')->name('national.get');
Route::get('national','NationalController@index')->name('national.index');
Route::post('national','NationalController@store')->name('national.store');
Route::get('national-ajax','NationalController@getnationalAjax')->name('ajax.getNational');
Route::get('nationals-list-ajax','NationalController@getnationalsListAjax')->name('ajax.getNationalsList');



/*Announcement routes*/
Route::get('announcement','AnnouncementController@index')->name('announcement.get');
Route::post('announcement','AnnouncementController@store')->name('announcement.post');



/*Presbytery routes*/
Route::get('presbytery/{id}','PresbyteryController@getPresbytery')->name('presbytery.get');
Route::get('presbytery','PresbyteryController@index')->name('presbytery.index');
Route::post('presbytery','PresbyteryController@store')->name('presbytery.store');
Route::get('get-presbytery-ajax','PresbyteryController@getPresbyteryAjax')->name('ajax.getPresbytery');
Route::get('presbytery-list-ajax','PresbyteryController@getpresbyteryListAjax')->name('ajax.getPresbyteryList');


/*District routes*/
Route::get('district/{id}','DistrictController@getDistrict')->name('district.get');
Route::get('district','DistrictController@index')->name('district.index');
Route::post('district','DistrictController@store')->name('district.store');
Route::get('get-district-ajax','DistrictController@getDistrictAjax')->name('ajax.getDistrict');
Route::get('district-list-ajax','DistrictController@getdistrictListAjax')->name('ajax.getDistrictList');


/*Local routes*/
Route::get('local/{id}','LocalController@getDistrict')->name('local.get');
Route::get('local','LocalController@index')->name('local.index');
Route::post('local','LocalController@store')->name('local.store');
Route::get('get-local-ajax','LocalController@getLocalAjax')->name('ajax.getLocal');
Route::get('local-list-ajax','LocalController@getdistrictListAjax')->name('ajax.getLocalList');
