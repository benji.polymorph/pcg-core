@extends('layouts.able')

@section('content')
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">

                <div class="row align-items-center">
                    <div class="col-md-8">
                        <div class="page-header-title">
                            <h4 class="m-b-10">PCG Local creation / editing</h4>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="index-2.html">
                                    <i class="feather icon-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Configurations</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#!">Locals</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page body start -->
                    <div class="page-body">
                        <div class="row">
                            <div class="col-xl-9">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Local List</h5>
                                        <span>List of Locals</span>
                                    </div>
                                    <div class="card-block">
                                        <div class="dt-responsive table-responsive">
                                            <table id="local-list-table" class="table table-striped table-bordered nowrap" style="width: 100% !important;">
                                                <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>District</th>
                                                    <th>Location</th>
                                                    <th>Short notes</th>
                                                    <th>action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Add/Edit Local</h5>
                                        <span>Kindly enter required details here</span>
                                    </div>
                                    <div class="card-block">
                                        <form action="{{route('local.store')}}" method="post">
                                            {{csrf_field()}}
                                            <input type="hidden" name="id" class="id">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="material-group material-group-primary">
                                                        <label class="float-label">Local Name</label>
                                                        <div class="form-group form-inverse">
                                                            <input type="text" name="local_name" id="local_name" class="form-control">
                                                            <span class="form-bar"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="material-group material-group-primary">
                                                        <label class="float-label">Local Location</label>
                                                        <div class="form-group form-inverse">
                                                            <input type="text" name="local_location" id="local_location" class="form-control">
                                                            <span class="form-bar"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="material-group material-group-primary">
                                                        <label class="float-label">Select District</label>
                                                        <div class="form-group form-inverse">
                                                            <select name="local_district" id="local_district" class="form-control">
                                                                <option value=""></option>
                                                                @foreach($districts as $district)
                                                                    <option value="{{$district->id}}">{{$district->district_name}}</option>
                                                                @endforeach
                                                            </select>
                                                            <span class="form-bar"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="material-group material-group-primary">
                                                        <label class="float-label">Short notes on local</label>
                                                        <div class="form-group form-inverse">
                                                            <textarea name="local_description" id="local_description" required class="form-control" rows="3"></textarea>
                                                            <span class="form-bar"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button class="btn btn-info btn-block">Submit</button>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- Page body end -->
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            var localListTable = $('#local-list-table').DataTable({"responsive": true,"paging": true,"ordering": true, "info": true, "select" : true,
                dom: '<"html5buttons"B>lTfgitp',
                "ajax": {"url": "{{route('ajax.getLocal')}}","type": "GET"},
                "columns": [{ "data": "local_name" },{"data": "local_district"},{ "data": "local_location" },{ "data": "local_description" },{"data":null}],
                "language": { "paginate": { "previous": '<i class="fa fa-angle-left"></i>', "next": '<i class="fa fa-angle-right"></i>'} },
                columnDefs:[
                    {
                        'targets':-1,
                        'defaultContent':"<button class=\"btn waves-effect localEdit waves-dark btn-warning btn-outline-warning btn-icon\"><i class=\"icofont icofont-warning-alt\"></i></button>"
                    }],
                /*"columnDefs": [ {
                    "targets": -1,
                    "data": null,
                    "defaultContent": "<button>Click!</button>"
                } ],*/
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]
            });
            $('#local-list-table tbody').on( 'click', 'button.localEdit', function () {
                var data = localListTable.row( $(this).parents('tr') ).data();
                $('#local_id').val(data.id);
                $('#local_location').val(data.local_location);
                $('#local_name').val(data.local_name);
                $('#local_district').val(data.local_district);
                $('#local_description').val(data.local_description);
            });
        });
    </script>
@endsection
