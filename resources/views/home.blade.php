@extends('layouts.able')

@section('content')
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <div class="page-header-title">
                            <h4 class="m-b-10">PCG Companion</h4>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="index-2.html">
                                    <i class="feather icon-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Advance Components</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#!">Pnotify</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page body start -->
                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <!-- Basic notifications card start -->
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Basic Notifications</h5>
                                    </div>
                                    <div class="card-block table-border-style">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <tbody>
                                                <tr>
                                                    <td>Primary Notice</td>
                                                    <td>
                                                        <button type="button" class="btn btn-primary btn-sm" id="pnotify-default">Click here! <i class="icofont icofont-play-alt-2"></i></button>
                                                    </td>
                                                    <td>Use id <code>pnotify-default</code> to use this style notification</td>
                                                </tr>
                                                <tr>
                                                    <td>Success Notice</td>
                                                    <td>
                                                        <button type="button" class="btn btn-success btn-sm" id="pnotify-success">Click here! <i class="icofont icofont-play-alt-2"></i></button>
                                                    </td>
                                                    <td>Use id <code>pnotify-success</code> to use this style notification</td>
                                                </tr>
                                                <tr>
                                                    <td>Info Notice</td>
                                                    <td>
                                                        <button type="button" class="btn btn-info btn-sm" id="pnotify-info">Click here! <i class="icofont icofont-play-alt-2"></i></button>
                                                    </td>
                                                    <td>Use id <code>pnotify-info</code> to use this style notification</td>
                                                </tr>
                                                <tr>
                                                    <td>Danger Notice</td>
                                                    <td>
                                                        <button type="button" class="btn btn-danger btn-sm" id="pnotify-danger">Click here! <i class="icofont icofont-play-alt-2"></i></button>
                                                    </td>
                                                    <td>Use id <code>pnotify-danger</code> to use this style notification</td>
                                                </tr>
                                                <!-- basic notification end -->
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- Basic notifications card end -->
                            </div>
                        </div>
                    </div>
                    <!-- Page body end -->
                </div>
            </div>
        </div>
    </div>
@endsection
