@extends('layouts.able')

@section('content')
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">

                <div class="row align-items-center">
                    <div class="col-md-8">
                        <div class="page-header-title">
                            <h4 class="m-b-10">PCG District creation / editing</h4>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="index-2.html">
                                    <i class="feather icon-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Configurations</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#!">Districts</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-xl-9">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>District List</h5>
                                        <span>List of Districts</span>
                                    </div>
                                    <div class="card-block">
                                        <div class="dt-responsive table-responsive">
                                            <table id="district-list-table" class="table table-striped table-bordered nowrap" style="width: 100% !important;">
                                                <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Presbytery</th>
                                                    <th>Location</th>
                                                    <th>Short notes</th>
                                                    <th>action(s)</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Add/Edit District</h5>
                                        <span>Kindly enter required details here</span>
                                    </div>
                                    <div class="card-block">
                                        <form action="{{route('district.store')}}" method="post">
                                            {{csrf_field()}}
                                            <input type="hidden" name="id" id="district_id">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="material-group material-group-primary">
                                                        <label class="float-label">District Name</label>
                                                        <div class="form-group form-inverse">
                                                            <input type="text" name="district_name" id="district_name" class="form-control">
                                                            <span class="form-bar"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="material-group material-group-primary">
                                                        <label class="float-label">District Location</label>
                                                        <div class="form-group form-inverse">
                                                            <input type="text" name="district_location" id="district_location" class="form-control">
                                                            <span class="form-bar"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="material-group material-group-primary">
                                                        <label class="float-label">Select Presbytery</label>
                                                        <div class="form-group form-inverse">
                                                            <select name="district_presbytery" id="district_presbytery" class="form-control">
                                                                <option value=""></option>
                                                                @foreach($presbyteries as $presbytery)
                                                                    <option value="{{$presbytery->id}}">{{$presbytery->presbytery_name}}</option>
                                                                @endforeach
                                                            </select>
                                                            <span class="form-bar"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="material-group material-group-primary">
                                                        <label class="float-label">Short notes on district</label>
                                                        <div class="form-group form-inverse">
                                                            <textarea name="district_description" id="district_description" required class="form-control" rows="3"></textarea>
                                                            <span class="form-bar"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button class="btn btn-info btn-block">Submit</button>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            var districtListTable = $('#district-list-table').DataTable({"responsive": true,"paging": true,"ordering": true, "info": true, "select" : true,
                dom: '<"html5buttons"B>lTfgitp',
                "ajax": {"url": "{{route('ajax.getDistrict')}}","type": "GET"},
                "columns": [{ "data": "district_name" },{"data": "district_presbytery"},{ "data": "district_location" },{ "data": "district_description" },{"data":null}],
                "language": { "paginate": { "previous": '<i class="fa fa-angle-left"></i>', "next": '<i class="fa fa-angle-right"></i>'} },
                columnDefs:[
                    {
                        'targets':-1,
                        'defaultContent':"<button class=\"btn waves-effect districtEdit waves-dark btn-warning btn-outline-warning btn-icon\"><i class=\"icofont icofont-warning-alt\"></i></button>"
                    }],
                /*"columnDefs": [ {
                    "targets": -1,
                    "data": null,
                    "defaultContent": "<button>Click!</button>"
                } ],*/
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]
            });
            $('#district-list-table tbody').on( 'click', 'button.districtEdit', function () {
                var data = districtListTable.row( $(this).parents('tr') ).data();
                $('#district_id').val(data.id);
                $('#district_location').val(data.district_location);
                $('#district_name').val(data.district_name);
                $('#district_presbytery').val(data.district_presbytery);
                $('#district_description').val(data.district_description);
            });
        });

    </script>
@endsection
