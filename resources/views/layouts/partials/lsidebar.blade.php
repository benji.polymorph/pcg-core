<nav class="pcoded-navbar">
    <div class="pcoded-inner-navbar main-menu">
        <div class="">
            <div class="main-menu-header">
                <img class="img-menu-user img-radius" src="{{ asset('files/assets/images/pcg-logo.png') }}" alt="User-Profile-Image">
                <div class="user-details">
                    <p id="more-details">Ascension Presby <i class="feather icon-chevron-down m-l-10"></i></p>
                </div>
            </div>
            <div class="main-menu-content">
                <ul>
                    <li class="more-details">
                        <a href="{{ route('home') }}">
                            <i class="feather icon-user"></i>View Profile
                        </a>
                        <a href="{{ route('home') }}">
                            <i class="feather icon-settings"></i>Settings
                        </a>
                        <a href="{{ route('home') }}">
                            <i class="feather icon-log-out"></i>Logout
                        </a>s
                    </li>
                </ul>
            </div>
        </div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="active">
                <a href="{{route('member.list')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                    <span class="pcoded-mtext">Dashboard</span>
                </a>
            </li>
        </ul>
        <div class="pcoded-navigation-label">Membership Management</div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="pcoded-hasmenu">
                <a href="javascript:void(0)" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="feather icon-users"></i></span>
                    <span class="pcoded-mtext">Member</span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="">
                        <a href="{{ route('member.create') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Create Member</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="{{ route('member.list') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Find Member</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="{{ route('member.tree') }}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Family Tree</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="pcoded-hasmenu">
                <a href="javascript:void(0)" class="waves-effect waves-dark">
									<span class="pcoded-micon">
										<i class="feather icon-airplay"></i>
									</span>
                    <span class="pcoded-mtext">Finance</span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="">
                        <a href="{{route('member.tithe','all')}}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Tithe</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="{{route('member.pledge',"all")}}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Pledge</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="{{route('offertory')}}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Offertory</span>
                        </a>
                    </li>

                </ul>
            </li>
        </ul>
        <div class="pcoded-navigation-label">Church Management</div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="pcoded-hasmenu">
                <a href="javascript:void(0)" class="waves-effect waves-dark">
									<span class="pcoded-micon">
										<i class="feather icon-message-square"></i>
									</span>
                    <span class="pcoded-mtext">Information Corner</span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="">
                        <a href="alert.html" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Announcements</span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="tooltip.html" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Bulk Messaging</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="pcoded-hasmenu">
                <a href="javascript:void(0)" class="waves-effect waves-dark">
									<span class="pcoded-micon">
										<i class="feather icon-activity"></i>
									</span>
                    <span class="pcoded-mtext">Activity</span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="">
                        <a href="{{route('almanac.index')}}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Almanac Upload</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="widget-data.html" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Preaching Plan</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="widget-chart.html" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Special Events</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        <div class="pcoded-navigation-label">Church Intelligence</div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="pcoded-hasmenu">
                <a href="javascript:void(0)" class="waves-effect waves-dark">
									<span class="pcoded-micon">
										<i class="feather icon-pie-chart"></i>
									</span>
                    <span class="pcoded-mtext">Reports</span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="">
                        <a href="alert.html" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Bed Management</span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="tooltip.html" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Building Management</span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="typography.html" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Room Management</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="pcoded-hasmenu">
                <a href="javascript:void(0)" class="waves-effect waves-dark">
									<span class="pcoded-micon">
										<i class="feather icon-bar-chart"></i>
									</span>
                    <span class="pcoded-mtext">Analytics</span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="">
                        <a href="alert.html" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Alert</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="breadcrumb.html" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Breadcrumbs</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>

        <div class="pcoded-navigation-label">Configuration</div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="pcoded-hasmenu">
                <a href="javascript:void(0)" class="waves-effect waves-dark">
									<span class="pcoded-micon">
										<i class="feather icon-users"></i>
									</span>
                    <span class="">User Mgt</span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="">
                        <a href="{{route('register')}}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">User Creation</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="pcoded-hasmenu">
                <a href="javascript:void(0)" class="waves-effect waves-dark">
									<span class="pcoded-micon">
										<i class="feather icon-users"></i>
									</span>
                    <span class="">Church Center</span>
                </a>
                <ul class="pcoded-submenu">
                    <li class=" ">
                        <a href="{{route('national.index')}}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">National Management</span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{route('presbytery.index')}}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Presbytery Management</span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{route('district.index')}}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">District Management</span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{route('local.index')}}" class="waves-effect waves-dark">
                            <span class="pcoded-mtext">Local Management</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>

    </div>
</nav>
