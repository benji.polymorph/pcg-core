@extends('layouts.able')

@section('content')

<div class="container">
    <div class="row ">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('User Role') }}</label>

                            <div class="col-md-6">
                                <select id="role" class="form-control{{ $errors->has('role') ? ' is-invalid' : '' }}" name="role" value="{{ old('role') }}" onchange="fillTerritory()" required>
                                    <option value=""></option>
                                    <option value="local_admin">Local Admin</option>
                                    <option value="district_admin">District Admin</option>
                                    <option value="presbytery_admin">Presbytery Admin</option>
                                    <option value="national_admin">National Admin</option>
                                    <option value="super_admin">Super Admin</option>
                                </select>

                                @if ($errors->has('role'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('role') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Territory') }}</label>

                            <div class="col-md-6">
                                <select class="js-data-example-ajax form-control" style="width: 100%" name="territory"></select>

                                @if ($errors->has('territory'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('territory') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    /*$('.js-data-example-ajax').select2({
        ajax: {
            url: "{{route('/')}}"+"/getTerritory/super_admin"
        }
    });*/
    function fillTerritory(){
        $('.js-data-example-ajax').val(null).trigger('change');
        let role= $('#role').val();
        let url="{{route('/')}}"+"/getTerritory/"+role;
        var studentSelect = $('.js-data-example-ajax');
        $.ajax({
            type: 'GET',
            url: url
        }).then(function (data) {
            // create the option and append to Select2
            data= JSON.parse(data);
            console.log(data)
            let options=[];
            for (let i=0;i<data.length;i++){
                var option = new Option(data[i].name, data[i].name, false, false);
                options.push(option);
            }
            //console.log(options)
            studentSelect.append(options).trigger('change');
            // manually trigger the `select2:select` event
            studentSelect.trigger({
                params: {
                    data: data
                }
            });
        });

            //alert(role);
            //alert(url);
/*
            $('.js-data-example-ajax').select2({
                ajax: {
                    url: url,
                    dataType: 'json',
                    type: 'GET',
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        };
                    }
                    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
                }
            });
*/
    }
</script>
@endsection
