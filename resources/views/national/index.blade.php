@extends('layouts.able')

@section('content')
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">

                <div class="row align-items-center">
                    <div class="col-md-8">
                        <div class="page-header-title">
                            <h4 class="m-b-10">PCG National creation / editing</h4>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="index-2.html">
                                    <i class="feather icon-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Configurations</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#!">National</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page body start -->
                    <div class="page-body">
                        <div class="row">
                            <div class="col-xl-9">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>National List</h5>
                                        <span>List of National</span>
                                    </div>
                                    <div class="card-block">
                                        <div class="dt-responsive table-responsive">
                                            <table id="national-list-table" class="table table-striped table-bordered nowrap" style="width: 100% !important;">
                                                <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Country</th>
                                                    <th>Short notes</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Add/Edit National</h5>
                                        <span>Kindly enter required details here</span>
                                    </div>
                                    <div class="card-block">
                                        <form action="{{route('national.store')}}" method="post">
                                            {{csrf_field()}}
                                            <input type="hidden" name="id" id="national_id">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="material-group material-group-primary">
                                                        <label class="float-label">National Name</label>
                                                        <div class="form-group form-inverse">
                                                            <input type="text" name="national_name" id="national_name" class="form-control">
                                                            <span class="form-bar"></span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12">
                                                    <div class="material-group material-group-primary">
                                                        <label class="float-label">Select Country</label>
                                                        <div class="form-group form-inverse">
                                                            <select name="national_country" id="national_country" class="form-control">
                                                                <option value=""></option>
                                                                <option value="1">Ghana</option>
                                                            </select>
                                                            <span class="form-bar"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="material-group material-group-primary">
                                                        <label class="float-label">Short notes on National</label>
                                                        <div class="form-group form-inverse">
                                                            <textarea name="national_description" id="national_description" required class="form-control" rows="3"></textarea>
                                                            <span class="form-bar"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button class="btn btn-info btn-block">Submit</button>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- Page body end -->
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            var nationalListTable = $('#national-list-table').DataTable({"responsive": true,"paging": true,"ordering": true, "info": true, "select" : true,
                dom: '<"html5buttons"B>lTfgitp',
                "ajax": {"url": "{{route('ajax.getNational')}}","type": "GET"},
                "columns": [{ "data": "national_name" },{"data": "national_country"},{ "data": "national_description" },{"data":null}],
                "language": { "paginate": { "previous": '<i class="fa fa-angle-left"></i>', "next": '<i class="fa fa-angle-right"></i>'} },
                columnDefs:[
                    {
                        'targets':-1,
                        'defaultContent':"<button class=\"btn waves-effect nationalEdit waves-dark btn-warning btn-outline-warning btn-icon\"><i class=\"icofont icofont-warning-alt\"></i></button>"
                    }],
                /*"columnDefs": [ {
                    "targets": -1,
                    "data": null,
                    "defaultContent": "<button>Click!</button>"
                } ],*/
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]
            });
            $('#national-list-table tbody').on( 'click', 'button.nationalEdit', function () {
                var data = nationalListTable.row( $(this).parents('tr') ).data();
                //console.log(data.id)
                $('#national_id').val(data.id);
                $('#national_country').val(data.national_country);
                $('#national_name').val(data.national_name);
                //$('#national_national').val(data.national_national);
                $('#national_description').val(data.national_description);
            });
        });

    </script>

@endsection
