@extends('layouts.able')

@section('content')
<link rel="icon" href="{{ asset('files/assets/images/favicon.ico') }}" type="image/x-icon">
<!-- Google font-->     <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
<!-- Required Fremwork -->
<link rel="stylesheet" type="text/css" href="{{ asset('files/bower_components/bootstrap/css/bootstrap.min.css') }}">
<!-- waves.css -->
<link rel="stylesheet" href="{{ asset('files/assets/pages/waves/css/waves.min.css') }}" type="text/css" media="all">
<!-- feather icon -->
<link rel="stylesheet" type="text/css" href="{{ asset('files/assets/icon/feather/css/feather.css') }}">
<!-- themify-icons line icon -->
<link rel="stylesheet" type="text/css" href="{{ asset('files/assets/icon/themify-icons/themify-icons.css') }}">
<!-- ico font -->
<link rel="stylesheet" type="text/css" href="{{ asset('files/assets/icon/icofont/css/icofont.css') }}">
<!-- flag icon framework css -->
<link rel="stylesheet" type="text/css" href="{{ asset('files/assets/pages/flag-icon/flag-icon.min.css') }}">
<!-- Font Awesome -->
<link rel="stylesheet" type="text/css" href="{{ asset('files/assets/icon/font-awesome/css/font-awesome.min.css') }}">
<!-- Syntax highlighter Prism css -->
<link rel="stylesheet" type="text/css" href="{{ asset('files/assets/pages/prism/prism.css') }}">
<!-- Style.css -->
<link rel="stylesheet" type="text/css" href="{{ asset('files/assets/css/style.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('files/assets/css/pages.css') }}">



<div class="pcoded-content">
    <!-- [ breadcrumb ] start -->
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <div class="page-header-title">
                        <h4 class="m-b-10">Almanac Upload</h4>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="">
                                <i class="feather icon-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Church</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#!">Almanac</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--
        [ breadcrumb ] end
    -->
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <!-- Page body start -->
                <div class="page-body">
                    <div class="row">

                        <div class="col-xl-9">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Default Calendar</h5>
                                    <span>This is the most basic eample with html template and events to be shown on the calendar.</span>
                                </div>
                                <div class="card-block">
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div id="example">

                                            </div>

                                        </div>
                                        <div class="col-lg-4">
                                            <div class="card">
                                                <div class="card-block">
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-xl-3">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Almanac Upload Entry</h5>
                                    <span>Kindly enter all required fields here</span>
                                </div>
                                <div class="card-block">
                                    <form action="{{route('almanac.store')}}" method="post">
                                        {{csrf_field()}}
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="material-group material-group-primary">
                                                    <div class="material-addone">
                                                        <i class="icofont icofont-user"></i>
                                                    </div>
                                                    <label class="float-label">Date</label>

                                                    <div class="form-group form-inverse">
                                                        <input type="date" name="almanacDate" class="form-control" required id="almanacDate">
                                                        <span class="form-bar"></span>
                                                    </div>
                                                </div>
                                                <div class="material-group material-group-primary">
                                                    <div class="material-addone">
                                                        <i class="icofont icofont-user"></i>
                                                    </div>
                                                    <label class="float-label">Occassion</label>

                                                    <div class="form-group form-inverse">
                                                        <input type="text" name="almanacOccassion"class="form-control"  required id="almanacOccassion">
                                                        <span class="form-bar"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="material-group material-group-primary">
                                                    <div class="material-addone">
                                                        <i class="icofont icofont-money"></i>
                                                    </div>
                                                    <label class="float-label">Theme</label>

                                                    <div class="form-group form-inverse">

                                                        <input type="text" name="almanacTheme" required class="form-control">
                                                        <span class="form-bar"></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- start cloned right side buttons element -->
                                            <div class="clone-link cloneya-wrap">
                                                <div class="toclone cloneya col-xl-12">
                                                    <button type="button" class=" clone btn btn-primary m-b-15">add new reading</button>
                                                    <button type="button" class=" delete  btn btn-danger m-b-15">delete reading</button>
                                                    <div class="j-row">
                                                        <div class="span6 unit">
                                                            <div class="input">
                                                                <input type="text" class="form-control" style="margin-bottom: 8px;" name="bibleReading[]" placeholder="Enter Bible Reading">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <button class="btn btn-info btn-block">Submit</button>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" src="{{ asset('files/bower_components/jquery/js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('files/bower_components/jquery-ui/js/jquery-ui.min.js') }}"></script>

<script src="jquery.calendar.js"></script>

<link rel="stylesheet" href="jquery.calendar.css">
<!-- waves js -->
<script src="{{ asset('files/assets/pages/waves/js/waves.min.js') }}"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="{{ asset('files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js') }}"></script>
<!-- modernizr js -->
<!-- Custom js -->
<!--
<script type="text/javascript" src="{{ asset('files/assets/pages/clndr-calendar/js/clndr-custom.js') }}"></script>
-->
<script src="{{ asset('files/assets/js/pcoded.min.js') }}"></script>
<script src="{{ asset('files/assets/js/vertical/vertical-layout.min.js') }}"></script>
<script src="{{ asset('files/assets/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>

<script>
    $(function () {
        $('#example').calendar(
            {
                months: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                days: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
                onSelect: function (event) {
                    console.log(event);
                    //$('.output').text(event.label);
                }

            }
        );
    });
</script>


<!-- Data Table Css -->

@endsection


