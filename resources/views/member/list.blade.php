@extends('layouts.able')

@section('content')
    <!-- Data Table Css -->
    <link rel="stylesheet" type="text/css" href="{{asset('files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('files/assets/pages/data-table/css/buttons.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}">
    <!-- Meta -->
    <!-- Style.css -->

    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <div class="page-header-title">
                            <h4 class="m-b-10">Find Member</h4>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="">
                                    <i class="feather icon-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Member</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#!">List</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page body start -->
                    <div class="page-body">
                        <div class="card">
                            <div class="card-header">
                                <h5>Member List</h5>
                                <span>List of registered Members</span>
                            </div>
                            <div class="card-block">
                                <div class="dt-responsive table-responsive">
                                    <table id="member-list-table" class="table table-striped table-bordered nowrap" style="width: 100% !important;">
                                        <thead>
                                        <tr>
                                            <th>Member ID</th>
                                            <th>Name</th>
                                            <th>Gender</th>
                                            <th>Contact Number</th>
                                            <th>Date of birth</th>
                                            <th>Email</th>
                                            <th>Residential Address</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Page body end -->
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            var memberListTable = $('#member-list-table').DataTable({"responsive": true,"paging": true,"ordering": true, "info": true, "select" : true,
                dom: '<"html5buttons"B>lTfgitp',
                "ajax": {"url": "{{route('ajax.get-members')}}","type": "GET"},
                "columns": [{ "data": "id" },{ "mData": function vehicle(data, type, dataToSet) {
                        return data.firstName + " " + data.surname + " " + data.otherNames ;
                    }
                },{"data": "gender"},{ "data": "phone" },{ "data": "dateOfBirth" },{ "data": "email" },{ "data": "residentialAddress" },{"data" : null}],
                "language": { "paginate": { "previous": '<i class="fa fa-angle-left"></i>', "next": '<i class="fa fa-angle-right"></i>'} },
                columnDefs:[
                    {
                    'targets':-1,
                    'defaultContent':"<button class=\"btn waves-effect memberBtn waves-dark btn-warning btn-outline-warning btn-icon\"><i class=\"icofont icofont-warning-alt\"></i></button>" +
                        "<button class=\"btn waves-effect titheBtn waves-dark btn-warning btn-outline-primary btn-icon\"><i class=\"icofont icofont-cabbage\"></i></button>" +
                        "<button class=\"btn waves-effect pledgeBtn waves-dark btn-warning btn-outline-success btn-icon\"><i class=\"icofont icofont-baby-milk-bottle\"></i></button>"
                    }],
                /*"columnDefs": [ {
                    "targets": -1,
                    "data": null,
                    "defaultContent": "<button>Click!</button>"
                } ],*/
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]
            });
            $('#member-list-table tbody').on( 'click', 'button.memberBtn', function () {
                var data = memberListTable.row( $(this).parents('tr') ).data();
                window.location.href = "{{route('/')}}"+"/mem/edit/"+data.id;

            } );
            $('#member-list-table tbody').on( 'click', 'button.titheBtn', function () {
                var data = memberListTable.row( $(this).parents('tr') ).data();
                window.location.href = "{{route('/')}}"+"/mem/tithe/"+data.id;

            } );
            $('#member-list-table tbody').on( 'click', 'button.pledgeBtn', function () {
                var data = memberListTable.row( $(this).parents('tr') ).data();
                window.location.href = "{{route('/')}}"+"/mem/pledge/"+data.id;

            } );
        });

    </script>

@endsection

