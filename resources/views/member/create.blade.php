@extends('layouts.able')

@section('content')
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <div class="page-header-title">
                            <h4 class="m-b-10">PCG Companion</h4>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="index-2.html">
                                    <i class="feather icon-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Home</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#!">Create Member</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page body start -->
                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <!-- Default select start -->
                                <form class="form-material" method="post" action="{{route('member.store')}}">
                                    {{csrf_field()}}
                                    <input type="hidden" name="id" value="{{$data->id}}">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5>Member Registration Details</h5>
                                            <span>Please supply all the required information</span>
                                        </div>
                                        <div class="card-block">


                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="material-group material-group-primary">
                                                        <div class="material-addone">
                                                            <i class="icofont icofont-layers"></i>
                                                        </div>
                                                        <div class="form-group form-static-label">
                                                            <select name="gender" id="gender" class="form-control">
                                                                <option value=""></option>
                                                                <option value="Male">Male</option>
                                                                <option value="Female">Female</option>
                                                            </select>
                                                            <span class="form-bar"></span>
                                                            <label class="float-label">Gender</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="material-group material-group-primary">
                                                        <div class="material-addone">
                                                            <i class="icofont icofont-signal"></i>
                                                        </div>
                                                        <div class="form-group form-static-label">
                                                            <input type="text" name="surname" value="{{$data->surname}}" class="form-control" required="">
                                                            <span class="form-bar"></span>
                                                            <label class="float-label">Last Name</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="material-group material-group-primary">
                                                        <div class="material-addone">
                                                            <i class="icofont icofont-signal"></i>
                                                        </div>
                                                        <div class="form-group form-static-label">
                                                            <input type="text" name="firstName" value="{{$data->firstName}}" class="form-control" required="">
                                                            <span class="form-bar"></span>
                                                            <label class="float-label">First Name</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="material-group material-group-primary">
                                                        <div class="material-addone">
                                                            <i class="icofont icofont-signal"></i>
                                                        </div>
                                                        <div class="form-group form-static-label">
                                                            <input type="text" name="otherNames" value="{{$data->otherNames}}" class="form-control">
                                                            <span class="form-bar"></span>
                                                            <label class="float-label">Middle Name(s)</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="material-group material-group-primary">
                                                        <div class="material-addone">
                                                            <i class="icofont icofont-email"></i>
                                                        </div>
                                                        <div class="form-group form-static-label">
                                                            <input type="email" name="email" class="form-control" value="{{$data->email}}" required="">
                                                            <span class="form-bar"></span>
                                                            <label class="float-label">Email Address</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="material-group material-group-primary">
                                                        <div class="material-addone">
                                                            <i class="icofont icofont-phone"></i>
                                                        </div>
                                                        <div class="form-group form-static-label">
                                                            <input type="tel" name="phone" value="{{$data->phone}}" class="form-control" required="">
                                                            <span class="form-bar"></span>
                                                            <label class="float-label">Contact Number</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="material-group material-group-primary">
                                                        <div class="material-addone">
                                                            <i class="icofont icofont-address-book"></i>
                                                        </div>
                                                        <div class="form-group form-static-label">
                                                            <input type="text" name="residentialAddress" value="{{$data->residentialAddress}}" class="form-control" required="">
                                                            <span class="form-bar"></span>
                                                            <label class="float-label">Residential Address</label>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="material-group material-group-primary">
                                                        <div class="material-addone">
                                                            <i class="icofont icofont-layers"></i>
                                                        </div>

                                                        <div class="form-group form-static-label">
                                                            <select name="generationalGroup" id="generationalGroup"  class="form-control">
                                                                <option value=""></option>
                                                                <option value="opt2">Basic</option>
                                                                <option value="opt3">Secondary</option>
                                                                <option value="opt3">Tertiary</option>
                                                            </select>
                                                            <span class="form-bar"></span>
                                                            <label class="float-label">Generational Group</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="material-group material-group-primary">
                                                        <div class="material-addone">
                                                            <i class="icofont icofont-layers"></i>
                                                        </div>

                                                        <div class="form-group form-static-label">
                                                            <select name="occupation" id="occupation"  class="form-control">
                                                                <option value=""></option>
                                                                <option value="opt2">Doctor</option>
                                                                <option value="opt3">Nurse</option>
                                                                <option value="opt3">Engineer</option>
                                                                <option value="opt3">Teacher</option>
                                                                <option value="opt3">Developer</option>
                                                                <option value="opt3">Architect</option>
                                                            </select>
                                                            <span class="form-bar"></span>
                                                            <label class="float-label">Occupation</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="material-group material-group-primary">
                                                        <div class="material-addone">
                                                            <i class="icofont icofont-meeting-add"></i>
                                                        </div>
                                                        <div class="form-group form-static-label">
                                                            <input type="date" name="dateOfBirth" value="{{$data->dateOfBirth}}" class="form-control">
                                                            <span class="form-bar"></span>
                                                            <label class="float-label">Date of Birth</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="material-group material-group-primary">
                                                        <div class="material-addone">
                                                            <i class="icofont icofont-layers"></i>
                                                        </div>
                                                        <div class="form-group form-static-label">
                                                            <select name="maritalStatus"  id="maritalStatus" class="form-control">
                                                                <option value=""></option>
                                                                <option value="opt2">Option 1</option>
                                                                <option value="opt3">Option 2</option>
                                                            </select>
                                                            <span class="form-bar"></span>
                                                            <label class="float-label">Martial Status</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="material-group material-group-primary">
                                                        <div class="material-addone">
                                                            <i class="icofont icofont-layers"></i>
                                                        </div>
                                                        <div class="form-group form-static-label">
                                                            <select name="educationalLevel" id="educationalLevel"  class="form-control">
                                                                <option value=""></option>
                                                                <option value="opt2">Primary</option>
                                                                <option value="opt3">Secondary</option>
                                                                <option value="opt3">Tertiary</option>
                                                            </select>
                                                            <span class="form-bar"></span>
                                                            <label class="float-label">Educational level</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="material-group material-group-primary">
                                                        <div class="material-addone">
                                                            <i class="icofont icofont-user"></i>
                                                        </div>
                                                        <div class="form-group form-static-label">
                                                            <input type="text" name="postalAddress" value="{{$data->postalAddress}}" class="form-control">
                                                            <span class="form-bar"></span>
                                                            <label class="float-label">Postal Address</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="material-group material-group-primary">
                                                        <div class="material-addone">
                                                            <i class="icofont icofont-layers"></i>
                                                        </div>
                                                        <div class="form-group form-static-label">
                                                            <select name="communicantStatus" id="communicantStatus"  class="form-control">
                                                                <option value=""></option>
                                                                <option value="opt2">Option 1</option>
                                                                <option value="opt3">Option 2</option>
                                                            </select>
                                                            <span class="form-bar"></span>
                                                            <label class="float-label">Communicant Status</label>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="material-group material-group-primary">
                                                        <div class="material-addone">
                                                            <i class="icofont icofont-layers"></i>
                                                        </div>
                                                        <div class="form-group form-static-label">
                                                            <select name="baptismStatus" id="baptismStatus"  class="form-control">
                                                                <option value=""></option>
                                                                <option value="opt2">Option 1</option>
                                                                <option value="opt3">Option 2</option>
                                                            </select>
                                                            <span class="form-bar"></span>
                                                            <label class="float-label">Baptism Status</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="material-group material-group-primary">
                                                        <div class="material-addone">
                                                            <i class="icofont icofont-image"></i>
                                                        </div>
                                                        <div class="form-group form-static-label">
                                                            <input type="image" name="photograph">
                                                            <span class="form-bar"></span>
                                                            <label class="float-label"></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <!-- Basic Inputs Validation start -->
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5>Image of Patient</h5>
                                                    <span>Most recent image of member</span>
                                                </div>
                                                <div class="card-block">
                                                    <div class="col-md-12">
                                                        <img src="{{asset('files/assets/images/patient_placeholder.jpg')}}" class="w-100 h-100 rounded-circle">
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Basic Inputs Validation end -->
                                        </div>

                                        <div class="col-sm-3">
                                            <!-- Basic Inputs Validation start -->
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5>Emergency Contact</h5>
                                                    <span>Input details about member's emergency contact here</span>
                                                </div>
                                                <div class="card-block">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="material-group material-group-primary">
                                                                <div class="material-addone">
                                                                    <i class="icofont icofont-user"></i>
                                                                </div>
                                                                <div class="form-group form-static-label">
                                                                    <input type="text" name="emergencyContactName" value="{{$data->emergencyContactName}}" class="form-control">
                                                                    <span class="form-bar"></span>
                                                                    <label class="float-label">Full Name</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="material-group material-group-primary">
                                                                <div class="material-addone">
                                                                    <i class="icofont icofont-contact-add"></i>
                                                                </div>
                                                                <div class="form-group form-static-label">
                                                                    <input type="text" name="emergencyContactTelephoneNumber" value="{{$data->emergencyContactTelephoneNumber}}" class="form-control">
                                                                    <span class="form-bar"></span>
                                                                    <label class="float-label">Contact Number</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="material-group material-group-primary">
                                                                <div class="material-addone">
                                                                    <i class="icofont icofont-layers"></i>
                                                                </div>
                                                                <div class="form-group form-static-label">
                                                                    <select name="emergencyContactRelationship" id="emergencyContactRelationship"   class="form-control">
                                                                        <option value=""></option>
                                                                        <option value="opt3">Sibling</option>
                                                                        <option value="opt3">Parent</option>
                                                                        <option value="opt3">Guardian</option>
                                                                    </select>
                                                                    <span class="form-bar"></span>
                                                                    <label class="float-label">Relationship</label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Basic Inputs Validation end -->
                                        </div>
                                        <div class="col-sm-3">
                                            <!-- Basic Inputs Validation start -->
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5>Next of Kin</h5>
                                                    <span>Input details about member's Next of kin here</span>
                                                </div>
                                                <div class="card-block">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="material-group material-group-primary">
                                                                <div class="material-addone">
                                                                    <i class="icofont icofont-user"></i>
                                                                </div>
                                                                <div class="form-group form-static-label">
                                                                    <input type="text" name="nextOfKinName" value="{{$data->nextOfKinName}}" class="form-control">
                                                                    <span class="form-bar"></span>
                                                                    <label class="float-label">Full Name</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="material-group material-group-primary">
                                                                <div class="material-addone">
                                                                    <i class="icofont icofont-contact-add"></i>
                                                                </div>
                                                                <div class="form-group form-static-label">
                                                                    <input type="text" name="nextOfKinTelephoneNumber" value="{{$data->nextOfKinTelephoneNumber}}" class="form-control">
                                                                    <span class="form-bar"></span>
                                                                    <label class="float-label">Contact Number</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="material-group material-group-primary">
                                                                <div class="material-addone">
                                                                    <i class="icofont icofont-layers"></i>
                                                                </div>
                                                                <div class="form-group form-static-label">
                                                                    <select name="nextOfKinRelationship" id="nextOfKinRelationship"  class="form-control">
                                                                        <option value=""></option>
                                                                        <option value="opt3">Sibling</option>
                                                                        <option value="opt3">Parent</option>
                                                                        <option value="opt3">Guardian</option>
                                                                    </select>
                                                                    <span class="form-bar"></span>
                                                                    <label class="float-label">Relationship</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Basic Inputs Validation end -->
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-block">
                                            <div class="float-right">
                                                <button class="btn btn-danger btn-sm"><i class="icofont icofont-refresh"></i>Reset form</button>
                                                <button class="btn btn-primary btn-sm"><i class="icofont icofont-check-circled"></i>Save member details</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                                <script>
                                    $('#gender').val('{{$data->gender}}');
                                    $('#nextOfKinRelationship').val('{{$data->nextOfKinRelationship}}');
                                    $('#emergencyContactRelationship').val('{{$data->emergencyContactRelationship}}');
                                    $('#baptismStatus').val('{{$data->baptismStatus}}');
                                    $('#communicantStatus').val('{{$data->communicantStatus}}');
                                    $('#maritalStatus').val('{{$data->maritalStatus}}');
                                    $('#occupation').val('{{$data->occupation}}');
                                    $('#generationalGroup').val('{{$data->generationalGroup}}');
                                    $('#educationalLevel').val('{{$data->educationalLevel}}');
                                </script>
                                <!-- Default select end -->
                            </div>
                        </div>
                    </div>
                    <!-- Page body end -->
                </div>
            </div>
        </div>
    </div>
@endsection

