@extends('layouts.able')

@section('content')
    <!-- Data Table Css -->
    <link rel="stylesheet" type="text/css" href="{{asset('files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('files/assets/pages/data-table/css/buttons.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}">
    <!-- Meta -->
    <!-- Style.css -->

    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <div class="page-header-title">
                            <h4 class="m-b-10">Tithes</h4>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="">
                                    <i class="feather icon-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Member</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#!">Tithe</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page body start -->
                    <div class="page-body">
                        <div class="row">
                            <div class="col-xl-9">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Tithe List</h5>
                                        <span>List of tithe payments made</span>
                                    </div>
                                    <div class="card-block">
                                        <div class="dt-responsive table-responsive">
                                            <table id="tithe-list-table" class="table table-striped table-bordered nowrap" style="width: 100% !important;">
                                                <thead>
                                                <tr>
                                                    <th>Tithe ID</th>
                                                    <th>Name</th>
                                                    <th>Phone</th>
                                                    <th>Amount</th>
                                                    <th>Date</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Tithe</h5>
                                        <span>Kindly enter tithe details here</span>
                                    </div>
                                    <div class="card-block">
                                        <form action="{{route('tithe.store')}}" method="post">
                                            {{csrf_field()}}
                                            @if($id!='all')
                                                <input type="hidden" name="memberID" value="{{$id}}">
                                            @endif
                                            <div class="row">
                                                @if($id=='all')
                                                    <div class="col-sm-12">
                                                    <div class="material-group material-group-primary">
                                                        <div class="material-addone">
                                                            <i class="icofont icofont-user"></i>
                                                        </div>
                                                        <label class="float-label">Select member</label>

                                                        <div class="form-group form-inverse">
                                                            <select class="js-data-example-ajax form-control" required style="width: 100%" name="memberID"></select>
                                                            <span class="form-bar"></span>
                                                        </div>
                                                        <script>
                                                            $('.js-data-example-ajax').select2({
                                                                ajax: {
                                                                    url: '{{route('ajax.loadMembers')}}',
                                                                    dataType: 'json',
                                                                    type: 'GET',

                                                                    processResults: function (data) {
                                                                        return {
                                                                            results: $.map(data, function (item) {
                                                                                return {
                                                                                    text: item.firstName+" "+item.surname+" "+item.otherNames,
                                                                                    id: item.id
                                                                                }
                                                                            })
                                                                        };
                                                                    }
                                                                    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
                                                                }
                                                            });
                                                        </script>
                                                    </div>
                                                </div>
                                                @endif
                                                <div class="col-sm-12">
                                                    <div class="material-group material-group-primary">
                                                        <div class="material-addone">
                                                            <i class="icofont icofont-money"></i>
                                                        </div>
                                                        <label class="float-label">Enter Amount</label>

                                                        <div class="form-group form-inverse">

                                                            <input type="number" name="amount" required class="form-control">
                                                            <span class="form-bar"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button class="btn btn-info btn-block">Submit</button>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- Page body end -->
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            var titheListTable = $('#tithe-list-table').DataTable({"responsive": true,"paging": true,"ordering": true, "info": true, "select" : true,
                dom: '<"html5buttons"B>lTfgitp',
                "ajax": {"url": "{{route('ajax.get-memberTithe',$id)}}","type": "GET"},
                "columns": [
                    { "data": "id" },
                    {
                        "mData": function vehicle(data, type, dataToSet) {
                            return data.member.firstName + " " + data.member.surname + " " + data.member.otherNames ;
                        }
                    },
                    {"data": "member.phone"},
                    { "data": "amount" },
                    { "data": "created_at" },
                ],
                "language": { "paginate": { "previous": '<i class="fa fa-angle-left"></i>', "next": '<i class="fa fa-angle-right"></i>'} },
                columnDefs:[
                    {
                        'targets':-1,
                        'defaultContent':"<button class=\"btn waves-effect memberBtn waves-dark btn-warning btn-outline-warning btn-icon\"><i class=\"icofont icofont-warning-alt\"></i></button>"
                    }],
                /*"columnDefs": [ {
                    "targets": -1,
                    "data": null,
                    "defaultContent": "<button>Click!</button>"
                } ],*/
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]
            });

        });

    </script>

@endsection

