@extends('layouts.able')

@section('content')
    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">

                <div class="row align-items-center">
                    <div class="col-md-8">
                        <div class="page-header-title">
                            <h4 class="m-b-10">PCG Presbytery creation / editing</h4>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="index-2.html">
                                    <i class="feather icon-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Configurations</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#!">Presbyteries</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page body start -->
                    <div class="page-body">
                        <div class="row">
                            <div class="col-xl-9">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Presbytery List</h5>
                                        <span>List of Presbyteries</span>
                                    </div>
                                    <div class="card-block">
                                        <div class="dt-responsive table-responsive">
                                            <table id="presbytery-list-table" class="table table-striped table-bordered nowrap" style="width: 100% !important;">
                                                <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>National</th>
                                                    <th>Location</th>
                                                    <th>action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Add/Edit Presbytery</h5>
                                        <span>Kindly enter required details here</span>
                                    </div>
                                    <div class="card-block">
                                        <form action="{{route('presbytery.store')}}" method="post">
                                            {{csrf_field()}}
                                            <input type="hidden" name="id" id="presbytery_id">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="material-group material-group-primary">
                                                        <label class="float-label">Presbytery Name</label>
                                                        <div class="form-group form-inverse">
                                                            <input type="text" name="presbytery_name" id="presbytery_name" class="form-control">
                                                            <span class="form-bar"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="material-group material-group-primary">
                                                        <label class="float-label">Presbytery Location</label>
                                                        <div class="form-group form-inverse">
                                                            <input type="text" name="presbytery_location" id="presbytery_location" class="form-control">
                                                            <span class="form-bar"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="material-group material-group-primary">
                                                        <label class="float-label">Select National</label>
                                                        <div class="form-group form-inverse">
                                                            <select name="presbytery_national" id="presbytery_national" class="form-control">
                                                                <option value=""></option>
                                                                @foreach($nationals as $national)
                                                                    <option value="{{$national->id}}">{{$national->national_name}}</option>
                                                                @endforeach
                                                            </select>
                                                            <script>

                                                            </script>
                                                            <span class="form-bar"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="material-group material-group-primary">
                                                        <label class="float-label">Short notes on presbytery</label>
                                                        <div class="form-group form-inverse">
                                                            <textarea name="presbytery_description" id="presbytery_description" required class="form-control" rows="3"></textarea>
                                                            <span class="form-bar"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button class="btn btn-info btn-block">Submit</button>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- Page body end -->
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            var districtListTable = $('#presbytery-list-table').DataTable({"responsive": true,"paging": true,"ordering": true, "info": true, "select" : true,
                dom: '<"html5buttons"B>lTfgitp',
                "ajax": {"url": "{{route('ajax.getPresbytery')}}","type": "GET"},
                "columns": [{ "data": "presbytery_name" },{"data": "presbytery_national"},{ "data": "presbytery_location" },{"data":null}],
                "language": { "paginate": { "previous": '<i class="fa fa-angle-left"></i>', "next": '<i class="fa fa-angle-right"></i>'} },
                columnDefs:[
                    {
                        'targets':-1,
                        'defaultContent':"<button class=\"btn waves-effect districtEdit waves-dark btn-warning btn-outline-warning btn-icon\"><i class=\"icofont icofont-warning-alt\"></i></button>"
                    }],
                /*"columnDefs": [ {
                    "targets": -1,
                    "data": null,
                    "defaultContent": "<button>Click!</button>"
                } ],*/
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]
            });
            $('#presbytery-list-table tbody').on( 'click', 'button.districtEdit', function () {
                var data = districtListTable.row( $(this).parents('tr') ).data();
                //console.log(data.id)
                $('#presbytery_id').val(data.id);
                $('#presbytery_location').val(data.presbytery_location);
                $('#presbytery_name').val(data.presbytery_name);
                $('#presbytery_national').val(data.presbytery_national);
                $('#presbytery_description').val(data.presbytery_description);
            });
        });

    </script>
    {{--$('#presbytery-list-table tbody').on( 'click', 'button.presbyteryEdit', function () {
    var data = presbyteryListTable.row( $(this).parents('tr') ).data();
    console.log(presbyteryListTable.row($(this).parents('tr'))).data();
    //alert(data.id)
    /* $('#presbytery_id').val(data.id);
    $('#presbytery_location').val(data.presbytery_location);
    $('#presbytery_name').val(data.presbytery_name);
    $('#presbytery_national').val(data.presbytery_national);
    $('#presbytery_description').val(data.presbytery_description);*/
    });--}}

@endsection
