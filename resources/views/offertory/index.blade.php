@extends('layouts.able')

@section('content')
    <!-- Data Table Css -->
    <link rel="stylesheet" type="text/css" href="{{asset('files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('files/assets/pages/data-table/css/buttons.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}">
    <!-- Meta -->
    <!-- Style.css -->

    <div class="pcoded-content">
        <!-- [ breadcrumb ] start -->
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <div class="page-header-title">
                            <h4 class="m-b-10">Offertory</h4>
                        </div>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="">
                                    <i class="feather icon-home"></i>
                                </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Church</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#!">Offertory</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ breadcrumb ] end -->
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page body start -->
                    <div class="page-body">
                        <div class="row">
                            <div class="col-xl-9">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Offertory List</h5>
                                        <span>List of Offertory made</span>
                                    </div>
                                    <div class="card-block">
                                        <div class="dt-responsive table-responsive">
                                            <table id="offertory-list-table" class="table table-striped table-bordered nowrap" style="width: 100% !important;">
                                                <thead>
                                                <tr>
                                                    <th>Record ID</th>
                                                    <th>Type</th>
                                                    <th>Amount</th>
                                                    <th>Entered By</th>
                                                    <th>Offertory Description</th>
                                                    <th>Date</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Offertory Entry</h5>
                                        <span>Kindly enter tithe details here</span>
                                    </div>
                                    <div class="card-block">
                                        <form action="{{route('offertory.store')}}" method="post">
                                            {{csrf_field()}}
                                            <div class="row">
                                                <div class="col-sm-12">
                                                        <div class="material-group material-group-primary">
                                                            <div class="material-addone">
                                                                <i class="icofont icofont-user"></i>
                                                            </div>
                                                            <label class="float-label">Offertory Type</label>

                                                            <div class="form-group form-inverse">
                                                                <select class="form-control" required style="width: 100%" name="offertoryType">
                                                                    <option value=""></option>
                                                                    <option value="Special Offertory">Special Offertory</option>
                                                                    <option value="Freewill Offertory">Freewill Offertory</option>
                                                                    <option value="Regular Offertory">Regular Offertory</option>
                                                                </select>
                                                                <span class="form-bar"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <div class="col-sm-12">
                                                    <div class="material-group material-group-primary">
                                                        <div class="material-addone">
                                                            <i class="icofont icofont-money"></i>
                                                        </div>
                                                        <label class="float-label">Enter Amount</label>

                                                        <div class="form-group form-inverse">

                                                            <input type="number" name="offertoryAmount" required class="form-control">
                                                            <span class="form-bar"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="material-group material-group-primary">
                                                        <div class="material-addone">
                                                            <i class="icofont icofont-brand-designfloat"></i>
                                                        </div>
                                                        <label class="float-label">Description of offertory</label>

                                                        <div class="form-group form-inverse">

                                                            <textarea name="offertoryDescription" required class="form-control" rows="3" required></textarea>
                                                            <span class="form-bar"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button class="btn btn-info btn-block">Submit</button>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- Page body end -->
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            var titheListTable = $('#offertory-list-table').DataTable({"responsive": true,"paging": true,"ordering": true, "info": true, "select" : true,
                dom: '<"html5buttons"B>lTfgitp',
                "ajax": {"url": "{{route('ajax.get-offertory')}}","type": "GET"},
                "columns": [
                    { "data": "offertoryID" },
                    {"data": "offertoryType"},
                    { "data": "offertoryAmount" },
                    { "data": "user.name" },
                    { "data": "offertoryDescription" },
                    { "data": "created_at" },
                ],
                "language": { "paginate": { "previous": '<i class="fa fa-angle-left"></i>', "next": '<i class="fa fa-angle-right"></i>'} },
                columnDefs:[
                    {
                        'targets':-1,
                        'defaultContent':"<button class=\"btn waves-effect memberBtn waves-dark btn-warning btn-outline-warning btn-icon\"><i class=\"icofont icofont-warning-alt\"></i></button>"
                    }],
                /*"columnDefs": [ {
                    "targets": -1,
                    "data": null,
                    "defaultContent": "<button>Click!</button>"
                } ],*/
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]
            });

        });

    </script>

@endsection
